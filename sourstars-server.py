#!/usr/bin/env python3

import argparse

from flask import Flask
from flask_socketio import SocketIO

from engineio.async_drivers import threading

from sourstars.config import data_path
from sourstars.data import DataModel
from sourstars.api import ServerAPI

app = Flask(__name__)
socketio = SocketIO(app, cors_allowed_origins="*", async_mode="threading")

data = DataModel(data_path)
api = ServerAPI(data, app)
socketio.on_namespace(api)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Sourstars Server')
    parser.add_argument('--host', dest='host', type=str, default='0.0.0.0',
                        help='server host (default: 0.0.0.0)')
    parser.add_argument('-p', '--port', dest='port', type=int, default='12345',
                        help='server port (default: 12345)')

    args = parser.parse_args()

    api.start_action_loop()
    socketio.run(app, host=args.host, port=args.port)

    data.save_to_path(data_path)

