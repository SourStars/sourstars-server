import json
import uuid
import time
import os
import queue
from typing import IO, Dict, Union

from .scan import scan


class DataModel:
    def __init__(self, path: str = ''):
        self.__projects = {}
        self.__actions = {}
        self.__action_queue = queue.Queue()

        if path and os.path.exists(path):
            self.read_from_path(path)

    @staticmethod
    def check_config(config: Dict) -> bool:
        if 'fetch' not in config or 'build' not in config or 'analyze' not in config:
            return False

        if 'method' not in config['fetch'] or 'method' not in config['build']:
            return False

        if 'checkers' not in config['analyze']:
            return False

        return True

    def __new_project(self, name: str, config: Dict) -> str:
        ident = uuid.uuid4().hex
        now = int(time.time())

        self.__projects.update({ident: {
            'name': name,
            'config': config,
            'create_time': now,
            'modify_time': now
        }})

        return ident

    def create_project(self, name: str, config: Dict) -> Union[str, None]:
        if not name:
            return None

        if not self.check_config(config):
            return None

        return self.__new_project(name, config)

    def edit_project(self, project_id: str, new_name: str = '', new_config: Dict = {}) -> bool:
        project = self.__projects.get(project_id)
        if project:
            now = int(time.time())

            if new_name:
                project['name'] = new_name
            if new_config:
                if not self.check_config(new_config) or new_config['fetch'] != project['config']['fetch']:
                    return False

                project['config'] = new_config

            project['modify_time'] = now

            return True

        return False

    def remove_project(self, project_id) -> bool:
        project = self.__projects.get(project_id)
        project_actions = [k for k, v in self.__actions.items() if v['project_id'] == project_id]

        if project and len(project_actions) == 0:
            self.__projects.pop(project_id)

            return True

        return False

    def __new_action(self, project_id) -> str:
        ident = uuid.uuid4().hex
        now = int(time.time())

        self.__actions.update({ident: {
            'project_id': project_id,
            'create_time': now,
            'start_time': 0,
            'finish_time': 0,
            'progress': 0,
            'log': '',
            'report': {},
            'status': 'prepared'
        }})

        self.__action_queue.put(ident)

        return ident

    def create_action(self, project_id) -> Union[str, None]:
        if self.__projects.get(project_id):
            return self.__new_action(project_id)

        return None

    def restart_action(self, action_id) -> Union[str, None]:
        action = self.__actions.get(action_id)
        if action:
            if action['status'] in ['prepared', 'scanning']:
                return None

            return self.__new_action(action['project_id'])

        return None

    def remove_action(self, action_id) -> bool:
        action = self.__actions.get(action_id)
        if action:
            if action['status'] in ['prepared', 'scanning']:
                return False

            self.__actions.pop(action_id)

            return True

        return False

    def run_action(self, action_id) -> Union[int, None]:
        action = self.__actions.get(action_id)
        if action:
            if action['status'] in ['scanning', 'aborted', 'finished']:
                return False

            project = self.__projects.get(action['project_id'])

            return scan(self, action_id, action, project['config'])

        return None

    def query_defect(self, action_id, defect_id) -> Union[Dict, None]:
        action = self.__actions.get(action_id)
        if action:
            if action['status'] in ['prepared', 'scanning', 'aborted']:
                return None

            report = action['report']
            if 'Defects' not in report or defect_id >= len(report['Defects']):
                return None

            defect = report['Defects'][defect_id]

            code_file = open(defect['Filename'], encoding='utf-8')
            code = code_file.read()
            code_file.close()

            return {
                'action_id': action_id,
                'defect_id': defect_id,
                'info': defect,
                'code': code
            }

        return None

    def action_loop(self):
        while True:
            action_id = self.__action_queue.get()
            self.run_action(action_id)

    def query_projects(self, *args):
        item = self.__projects
        for i in args:
            item = item[i]

        return item

    def query_actions(self, *args):
        item = self.__actions
        for i in args:
            item = item[i]

        return item

    def read(self, file: IO):
        data = json.load(file)
        self.__projects = data['projects']
        self.__actions = data['actions']

        action_list = sorted(
            [k for k, v in self.__actions.items() if v['status'] == 'prepared'],
            key=lambda k: self.__actions[k]['create_time']
        )
        for i in action_list:
            self.__action_queue.put(i)

    def save(self, file: IO):
        scanning_actions = [k for k, v in self.__actions.items() if v['status'] == 'scanning']
        for i in scanning_actions:
            action = self.__actions[i]
            action['status'] = 'aborted'
            action['finish_time'] = int(time.time())

        json.dump({
            'projects': self.__projects,
            'actions': self.__actions
        }, file, indent=2)

    def read_from_path(self, path: str):
        file = open(path)
        self.read(file)
        file.close()

    def save_to_path(self, path: str):
        file = open(path, 'w')
        self.save(file)
        file.close()

