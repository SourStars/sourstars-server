import tempfile
import subprocess
import json
from typing import Dict, Callable, Any
import os
import time

from .config import scan_path
from .utils import get_progress, emit_actions, emit_log_line, emit_progress


def scan(data, action_id: str, action: Dict, config: Dict,
         progress_emit: Callable[[str, float], None] = lambda aid, progress: emit_progress(aid, progress),
         log_line_emit: Callable[[str, str], None] = lambda aid, log_line: emit_log_line(aid, log_line),
         actions_emit: Callable[[Any], None] = lambda d: emit_actions(d)
         ) -> int:
    action['status'] = 'scanning'
    action['start_time'] = int(time.time())
    actions_emit(data)

    temp_fd, temp_filename = tempfile.mkstemp()
    p = subprocess.Popen([scan_path, '-', '--result-output', temp_filename], encoding='utf-8',
                         stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)

    p.stdin.write(json.dumps(config))
    p.stdin.close()

    line = p.stdout.readline()
    while line != '':
        progress = get_progress(line)
        if progress:
            action['progress'] = progress
            progress_emit(action_id, progress)

        action['log'] += line
        log_line_emit(action_id, line)
        line = p.stdout.readline()

    ret_code = p.wait()
    if ret_code == 0:
        report_path = json.load(os.fdopen(temp_fd))['report_path']

        report_file = open(report_path)
        report = json.load(report_file)
        report_file.close()

        action['report'] = report

        action['status'] = 'finished'
    else:
        action['status'] = 'aborted'

    action['finish_time'] = int(time.time())
    actions_emit(data)

    return ret_code
