import shutil
import sys
import os

scan_path = shutil.which('sourstars-scan')

if getattr(sys, 'frozen', False):
    application_path = os.path.dirname(sys.executable)
elif __file__:
    application_path = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
else:
    application_path = '.'

data_dir = f'{application_path}/data'
if not os.path.exists(data_dir):
    os.mkdir(data_dir)

data_path = f'{data_dir}/data.json'
