from typing import Union, Dict
from flask_socketio import emit


def starts_with(string: str, pattern: str) -> Union[str, None]:
    if string.startswith(pattern):
        return string[len(pattern):]

    return None


def get_progress(line: str) -> Union[float, None]:
    sub_line = starts_with(line, '>sourstars')
    if sub_line:
        com_line = starts_with(sub_line, '>fetch')
        if com_line:
            if com_line.startswith('> git'):
                return 1
        com_line = starts_with(sub_line, '>build')
        if com_line:
            if com_line.startswith('> cmake'):
                return 5
            if com_line.startswith('> make') or com_line.startswith('> mingw32-make'):
                return 10
        com_line = starts_with(sub_line, '>analyze')
        if com_line:
            if com_line.startswith('> ('):
                info = com_line.split()
                index, total = int(info[2]), int(info[4])
                return 20 + (index - 1) * (75 / total)
            if com_line.startswith('> done'):
                return 95
        if sub_line.startswith('>result'):
            return 100

    return None


def emit_projects(data, broadcast: bool = True):
    return emit('projects', data.query_projects(), broadcast=broadcast, namespace='/')


def emit_actions(data, broadcast: bool = True):
    return emit('actions', data.query_actions(), broadcast=broadcast, namespace='/')


def emit_progress(action_id: str, progress: float, broadcast: bool = True):
    return emit('progress', {'action_id': action_id, 'value': progress}, broadcast=broadcast, namespace='/')


def emit_log_line(action_id: str, log_line: str, broadcast: bool = True):
    return emit('log_line', {'action_id': action_id, 'value': log_line}, broadcast=broadcast, namespace='/')


def emit_defect(action_id: str, defect_id: int, info: Dict, code: str):
    return emit('defect', {'action_id': action_id, 'defect_id': defect_id, 'info': info, 'code': code})


def emit_error(event: str, msg: str = ''):
    return emit('error', {'event': event, 'message': msg})
