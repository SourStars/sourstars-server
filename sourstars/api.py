from flask import Flask
from flask_socketio import Namespace

from typing import Dict
from threading import Thread

from .data import DataModel
from .utils import emit_actions, emit_projects, emit_error, emit_defect


class ServerAPI(Namespace):
    def __init__(self, data: DataModel, app: Flask):
        super(ServerAPI, self).__init__('/')
        self.data = data
        self.app = app

    def on_connect(self):
        emit_projects(self.data, broadcast=False)
        emit_actions(self.data, broadcast=False)

    def on_disconnect(self):
        pass

    def __process(self, method: str, args: Dict, success_callback, error_message: str):
        if self.data.__class__.__dict__[method](self.data, **args):
            success_callback(self.data)
        else:
            emit_error(method, error_message)

    def process_project(self, method: str, args: Dict, error_message: str):
        self.__process(method, args, emit_projects, error_message)

    def process_action(self, method: str, args: Dict, error_message: str):
        self.__process(method, args, emit_actions, error_message)

    def on_create_project(self, args):
        self.process_project('create_project', args, 'expected non-blank name and valid config')

    def on_edit_project(self, args):
        self.process_project('edit_project', args,
                             'expected valid id, non-blank name and valid config with constant `fetch` node')

    def on_remove_project(self, args):
        self.process_project('remove_project', args, 'expected valid project id without related action')

    def on_create_action(self, args):
        self.process_action('create_action', args, 'expected valid project id')

    def on_restart_action(self, args):
        self.process_action('restart_action', args, 'expected valid action id with aborted or finished status')

    def on_remove_action(self, args):
        self.process_action('remove_action', args, 'expected valid action id with aborted or finished status')

    def on_query_defect(self, args):
        defect = self.data.query_defect(**args)
        if defect:
            emit_defect(**defect)
        else:
            emit_error('query_defect', 'expected valid action id with finished status and valid defect id')

    def start_action_loop(self):
        def action_loop(m: DataModel):
            with self.app.app_context():
                m.action_loop()

        t = Thread(target=action_loop, args=(self.data,), daemon=True)
        t.start()

        return t
